class Movie {
    title: string
    year: number
    director: string
    casting: string[]
    description: string
    image: string
    favorite: boolean
    duration: number
    rate?: number
  
    constructor(title: string, year: number, director: string, casting: string[], description: string, image: string, favorite: boolean, duration:number, rate:number) {
      this.title = title;
      this.year = year;
      this.director = director;
      this.casting = casting;
      this.description = description;
      this.image = image;
      this.favorite = favorite;
      this.duration = duration;
      this.rate;

    }
}

const avatar = new Movie ("Avatar",2009,  "James Cameron", ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"], "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.", "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg", false, 162, 7.9,
)
const threeHundred = new Movie ("300", 2006,"Zack Snyder", ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],  "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",  "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg", false, 117, 
 7.7,
)

const horrorMovie = new Movie ("The Avengers", 2012, "Joss Whedon", ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"], "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.", "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg", true, 143,8.1
)

let movies = JSON.parse(localStorage.getItem("movies")) || [
    avatar, threeHundred, horrorMovie
]

let containerMovies = document.querySelector('#container-movies');

let nodeMovie: any;

window.addEventListener("load", (event) => {
  displayMovies();
});


function displayMovies() {

  for (let movie of movies) {

   nodeMovie = document.createElement('article');
    containerMovies.append(nodeMovie)

    let image = document.createElement('img');
    image.src = movie.image;
    nodeMovie?.append(image);
    
    let title = document.createElement('h1');
    title.textContent = movie.title;
    nodeMovie?.append(title);

    let year = document.createElement('p');
    year.textContent = movie.year.toString();
    nodeMovie?.append(year);

    let casting = document.createElement('ul');
    casting.textContent = movie.casting.toString();
    nodeMovie?.append(casting);

    let favorite = document.createElement('p');
    favorite.textContent = movie.favorite.toString();
    nodeMovie?.append(favorite);

  
  }


  let moviesToString = JSON.stringify(movies);
  localStorage.setItem("movies", moviesToString);

  console.log("movies", moviesToString);

}

// récupérer le bouton
const buttonSend = document.querySelector("#movie-submit");

// écouter l'action au clic sur le bouton
buttonSend.addEventListener('click', function(event) {
  // empêcher l'action par défaut (rafraîchir la page -> l'envoi du formulaire)
  event.preventDefault();

  // récupérer le formulaire
  const movieForm: MovieForm = document.querySelector("#movie-form");

  interface MovieForm extends Element  {
    movieTitle: HTMLInputElement,
    movieYear: HTMLInputElement,
    movieDirector: HTMLInputElement,
    movieCasting: HTMLInputElement,
    movieImage: HTMLInputElement,
    movieDescription: HTMLInputElement,
    movieFavorite: HTMLInputElement
  }

  // récupérer les valeurs du formulaire (nom, prénom, age)
  const titleValue = movieForm.movieTitle.value;
  const yearValue = +movieForm.movieYear.value;
  const directorValue = movieForm.movieDirector.value;
  const castingValue = movieForm.movieCasting.value.split(',');
  const descriptionValue = movieForm.movieDescription.value;
  const imageValue = movieForm.movieImage.value;
  const durationValue = +movieForm.movieDirector.value;
  const favoriteValue = Boolean(movieForm.movieFavorite.value);

  // créer un objet littéral avec ces valeurs
  // movie
  const movie = {
    title: titleValue,
    image: imageValue,
    year: yearValue,
    director: directorValue,
    casting: castingValue,
    description: descriptionValue,
    duration: durationValue,
    favorite: favoriteValue
  }

 // nodeMovie.textContent = '';
  movies.push(movie)
  let moviesToString = JSON.stringify(movies);
  localStorage.setItem("movies", moviesToString);

  displayMovies();
});
