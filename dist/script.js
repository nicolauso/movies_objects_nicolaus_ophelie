var Movie = /** @class */ (function () {
    function Movie(title, year, director, casting, description, image, favorite, duration, rate) {
        this.title = title;
        this.year = year;
        this.director = director;
        this.casting = casting;
        this.description = description;
        this.image = image;
        this.favorite = favorite;
        this.duration = duration;
        this.rate;
    }
    return Movie;
}());
var avatar = new Movie("Avatar", 2009, "James Cameron", ["Sam Worthington", "Zoe Saldana", "Sigourney Weaver", "Stephen Lang"], "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.", "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg", false, 162, 7.9);
var threeHundred = new Movie("300", 2006, "Zack Snyder", ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"], "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.", "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg", false, 117, 7.7);
var horrorMovie = new Movie("The Avengers", 2012, "Joss Whedon", ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"], "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.", "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg", true, 143, 8.1);
var movies = JSON.parse(localStorage.getItem("movies")) || [
    avatar, threeHundred, horrorMovie
];
var containerMovies = document.querySelector('#container-movies');
var nodeMovie;
window.addEventListener("load", function (event) {
    displayMovies();
});
function displayMovies() {
    for (var _i = 0, movies_1 = movies; _i < movies_1.length; _i++) {
        var movie = movies_1[_i];
        nodeMovie = document.createElement('article');
        containerMovies.append(nodeMovie);
        var image = document.createElement('img');
        image.src = movie.image;
        nodeMovie === null || nodeMovie === void 0 ? void 0 : nodeMovie.append(image);
        var title = document.createElement('h1');
        title.textContent = movie.title;
        nodeMovie === null || nodeMovie === void 0 ? void 0 : nodeMovie.append(title);
        var year = document.createElement('p');
        year.textContent = movie.year.toString();
        nodeMovie === null || nodeMovie === void 0 ? void 0 : nodeMovie.append(year);
        var casting = document.createElement('ul');
        casting.textContent = movie.casting.toString();
        nodeMovie === null || nodeMovie === void 0 ? void 0 : nodeMovie.append(casting);
        var favorite = document.createElement('p');
        favorite.textContent = movie.favorite.toString();
        nodeMovie === null || nodeMovie === void 0 ? void 0 : nodeMovie.append(favorite);
    }
    var moviesToString = JSON.stringify(movies);
    localStorage.setItem("movies", moviesToString);
    console.log("movies", moviesToString);
}
// récupérer le bouton
var buttonSend = document.querySelector("#movie-submit");
// écouter l'action au clic sur le bouton
buttonSend.addEventListener('click', function (event) {
    // empêcher l'action par défaut (rafraîchir la page -> l'envoi du formulaire)
    event.preventDefault();
    // récupérer le formulaire
    var movieForm = document.querySelector("#movie-form");
    // récupérer les valeurs du formulaire (nom, prénom, age)
    var titleValue = movieForm.movieTitle.value;
    var yearValue = +movieForm.movieYear.value;
    var directorValue = movieForm.movieDirector.value;
    var castingValue = movieForm.movieCasting.value.split(',');
    var descriptionValue = movieForm.movieDescription.value;
    var imageValue = movieForm.movieImage.value;
    var durationValue = +movieForm.movieDirector.value;
    var favoriteValue = Boolean(movieForm.movieFavorite.value);
    // créer un objet littéral avec ces valeurs
    // movie
    var movie = {
        title: titleValue,
        image: imageValue,
        year: yearValue,
        director: directorValue,
        casting: castingValue,
        description: descriptionValue,
        duration: durationValue,
        favorite: favoriteValue
    };
    // nodeMovie.textContent = '';
    movies.push(movie);
    var moviesToString = JSON.stringify(movies);
    localStorage.setItem("movies", moviesToString);
    displayMovies();
});
